<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Chat</title>
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <style>
        .list-group{
            overflow-y: scroll;
            min-height: 200px;
            max-height: 200px;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="row" id="app">
        <div class="col-4 offset-4 col-sm-10 offset-sm-1">
            <li class="list-group-item active" aria-current="true">Chat room <span class="float-end">@{{totalUser}} users</span></li>
            <ul class="list-group" v-chat-scroll>
                <message v-for="(value,index) in chat" :key=value.index :color=color[index] :user=user[index] :time=time[index]>
                    @{{ value }}
                </message>
            </ul>
            <small v-if="typing">typing...</small>
            <input type="text" class="form-control" placeholder="Type your message here ..." @keyup.enter="send" v-model="message">
        </div>
    </div>
</div>
<script src="{{asset('js/app.js')}}"></script>
</body>
</html>
