<?php

namespace App\Http\Controllers;

use App\Events\ChatEvent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class ChatController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function send(Request $request){
        $user = Auth::user();
        $this->saveToSession($request);
        event(new ChatEvent($user,$request->message));
    }
    public function oldMessages()
    {
        return Session::get('chat');
    }
    public function saveToSession(Request $request){
        Session::put('chat',$request->chat);
    }
    public function delete(){
        Session::forget('chat');
        return "successfully deleted";
    }
}
