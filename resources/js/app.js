require('./bootstrap');

import Vue from "vue";
window.Vue = require('vue').default;
import 'bootstrap';

import VueChatScroll from "vue-chat-scroll";
Vue.use(VueChatScroll);

import Toaster from 'v-toaster'
import 'v-toaster/dist/v-toaster.css'
Vue.use(Toaster,{timeout:3000})

Vue.component('chat',require('./component/chat').default);

const app = new Vue({
    el:'#app'
});
